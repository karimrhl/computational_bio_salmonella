# Computational_BIO_SALMONELLA



# Computational Biology Salmonella Outbreak



### Getting started

This tool takes 2 different fasta files(Wild and Variant) as input, and gives in output a list of SNPs.


### Install Requirements:

```
$ pip install -r requirements.txt
```

### Execution

After installing the requirements, open a terminal and launch the project as:

```
$ python3 main.py first_file.fasta second_file.fasta -k 40
```

Or, if it does not work, 
```
$ python main.py first_file.fasta second_file.fasta -k 40
```

where:


first_file.fasta is the Wild file

second_file.fasta is the Variant file

k is the lenght of the k-mer


After launched, the tool finds the aboudance of the k-mers in file1 and file2. The data files can be downloaded here: https://cloud-ljk.imag.fr/index.php/s/HkxDLozHRcqBcqz

Note: Data should be in the same directory with the python files.


### Results 


1- SNP:

W: TTACATGCCAATACAATGTAGGCTGCTCTACACCTAGCTTCTGGGCGAG`TTT`ACGGGTTGTTAAACCTTCGATTCCGACCTCATTAAGCAGCTCTAATGCG

V: TTACATGCCAATACAATGTAGGCTGCTCTACACCTAGCTTCTGGGCGAG`GGG`ACGGGTTGTTAAACCTTCGATTCCGACCTCATTAAGCAGCTCTAATGCG  


------------------------------------------------------

2- SNP

W: CGCATTAGAGCTGCTTAATGAGGTCGGAATCGAAGGTTTAACAACCCGT`AAA`CTCGCCCAGAAGCTAGGTGTAGAGCAGCCTACATTGTATTGGCATGTAA

V: CGCATTAGAGCTGCTTAATGAGGTCGGAATCGAAGGTTTAACAACCCGT`CCC`CTCGCCCAGAAGCTAGGTGTAGAGCAGCCTACATTGTATTGGCATGTAA

	
### Authors and acknowledgment
- Karim MIKE Rahhal
- Sara Avesani
- Albi Isufaj
