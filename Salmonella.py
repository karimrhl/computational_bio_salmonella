import numpy as np
import matplotlib.pyplot as plt
from scipy.special import factorial
from scipy.stats import binom
from scipy.stats import poisson
from Bio import SeqIO 
import timeit 
import copy 
from collections import defaultdict
from Levenshtein import distance



# Parameters of the model
G = 5e6*2
r = 2.5e2
eps = 0.01
N = 1993166
k = 40

# Probabilities we are interested in
P_x = (r-k+1)/(G-r+1)
P_ef = ((1-eps)**k)*((r-k+1)/(G-r+1))
P_1err = k*eps * k*((1-eps)**(k-1))*((r-k+1)/(G-r+1))


# plot Poisson distribition (x = np.arange(0, 350)   mu = N*P_x)

def plot_Poisson(x,mu):
    pmf = poisson.pmf(x, mu)
    pmf = np.round(pmf, 5)
    plt.plot(x, pmf)
    plt.show()


# dictionary(file, k): takes in input a FASTA file and an integer number k and gives in output a dicrionary with the k-length 
# subsequences as keys and how many times they appear as values.

def dictionary(file, k): 
    time_start = timeit.default_timer() 
    freqdic = defaultdict(int) 
    for seq_record in SeqIO.parse(file, "fasta"): 
        dnaseq = str(seq_record.seq) 
        for i in range(len(dnaseq)-k+1): 
            kmer = dnaseq[i:i+k] 
            freqdic[kmer] +=1 
 
    time_stop = timeit.default_timer() 
    time_read = time_stop - time_start 
    print(f"Time required for reading the file and store it in a dictionary is {(time_read)/60:.2f} minutes") 
    return freqdic





def occurences(freq_dict) :
    freq = {}
    occurences_list = list(freq_dict.values())
    for occurence in occurences_list :
        if occurence not in freq :
            freq[occurence] = 1
        else :
            freq[occurence] += 1
    # sort the freq dictionnary by keys 
    freq = dict(sorted(freq.items(), key=lambda item: item[0]))
    return freq




def plot_occurences(occurence_list):
    plt.plot(list(occurence_list.keys()), list(occurence_list.values()))
    plt.yscale('log')
    plt.xlabel('Abundance')
    plt.ylabel('Number of kmers')
    plt.title('Occurences of kmers')
    plt.legend(['Wild type', 'Variant type'], loc='upper right')
    # save the graph
    plt.savefig('occurences.png')
    plt.show()



#wild_dict = dictionary("salmonella-enterica.reads.fna", 40)
#variant_dict = dictionary("salmonella-enterica-variant.reads.fna", 40)

#plot_occurences(occurences(wild_dict))
#plot_occurences(occurences(variant_dict))



# remove_seq_errors(dictionary, threshold): takes in input the dictionary of the k-mers and their frequencies
# and a frequency threshold (that represent the number below which we consider a k-mer affected by sequencing error)
# and gives in output the new smaller error free dictionary 

def remove_seq_errors(dictionary, threshold):
    print(f'Initial dimension of the dictionary: {len(dictionary)}')
    time_start = timeit.default_timer() 
    for key,value in dict(dictionary).items():
        if value <= threshold:
            del dictionary[key]
    time_stop = timeit.default_timer() 
    time_remove = time_stop - time_start 
    print(f"Time required for removing the sequencing errors is {(time_remove):.2f} seconds")
    print(f'Final dimension of the dictionary: {len(dictionary)}')
    return dictionary


#wild_dict_ef = remove_seq_errors(wild_dict, 10)
#variant_dict_ef = remove_seq_errors(variant_dict, 10)

def remove_equal_keys(dict1,dict2):
    copied_dict1 = copy.deepcopy(dict1)
    copied_dict2 = copy.deepcopy(dict2)
    print(f'Initial dimension of the dictionary 1: {len(dict1)}')
    print(f'Initial dimension of the dictionary 2: {len(dict2)}')
    time_start = timeit.default_timer() 
    for key in copied_dict1:
        if key in copied_dict2:
            del dict1[key]
            del dict2[key]
    time_stop = timeit.default_timer() 
    time_remove = time_stop - time_start 
    print(f"It takes:{(time_remove):.2f} seconds to remove the equal items from the dictionaries ")
    print(f'Final dimension of the dictionary 1: {len(dict1)}')
    print(f'Final dimension of the dictionary 2: {len(dict2)}')
    return dict1, dict2


#wild_dict_final, variant_dict_final = remove_equal_keys(wild_dict_ef,variant_dict_ef)

# merge(seq1,seq2): gives in output the maximum number of letters we can allign. It only look at the case in which 
# the end of the first sequence overlaps the begining of the second one. Example: seq1='ABCD', seq2='CDEF', output = 2

def merge(seq1,seq2):
    l = min(len(seq1),len(seq2))
    m=np.zeros(l)
    # case 1: end of the first, beginning of the second
    for i in range(1,l):
        if seq1[-i:] == seq2[:i]:
            m[i-1] = i
        else:
            m[i-1] = 0
    M = int(max(m))
    return M



# max_allignemt(seq1,seq2, threshold): takes in input two sequences and gives in output the longest alligned sequence between the two.
# The threshold can be useful to improve the allignment by putting a minumum number of letters we want to intersect.

def max_allignemt(seq1,seq2, threshold):
    M=0
    M12 = merge(seq1,seq2)
    M21 = merge(seq2,seq1)
    if (M12 > M21) and (M12 >= threshold):
        M = M12
        aligned_seq = seq1 + seq2[M12:]
    elif (M21 > M12) and (M21 >= threshold):
        M = M12
        aligned_seq = seq2 + seq1[M21:]
    else: aligned_seq = ''
    return aligned_seq




# Given a list of k mers, this function gives in output all the maxima alligments that we can create with these k mers.
# The threshold can be useful do improve the allignment by putting a minumum number of letters we want to intersect.

def assembler(keys_list,threshold):
    removed_keys = []
    no_intersection = []
    first_seq = keys_list[0]
    for k in range(len(keys_list)):
        if keys_list[k] not in removed_keys and keys_list[k] not in no_intersection and keys_list[k] !=  first_seq:
            a = max_allignemt(keys_list[k],first_seq, threshold)
            if a != '':
                removed_keys.append(first_seq)
                removed_keys.append(keys_list[k])
                first_seq = a
            else: 
                no_intersection.append(keys_list[k])
    return first_seq, no_intersection




def color_mutations(mutations):
    for j in range(len(mutations)):
        print("For the {}-th mutation:".format(j+1))
        L = []
        for i in range(len(mutations[j][0])):
            if mutations[j][0][i] != mutations[j][1][i]:
                print("\033[1;31;40m", mutations[j][0][i], end = "")
                L.append(i)
            else:
                print("\033[1;37;40m", mutations[j][0][i], end = "")
        
        print()
        # print the second sequence
        for i in range(len(mutations[j][1])):
            if i in L:
                print("\033[1;32;40m", mutations[j][1][i], end = "")
            else:
                print("\033[1;37;40m", mutations[j][1][i], end = "")
        print()

