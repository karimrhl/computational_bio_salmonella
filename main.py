
import Salmonella as S 
import argparse


## -- INPUT PARAMETERS -- ##

parser = argparse.ArgumentParser()
parser.add_argument('file1', help='file with the Wild genome')
parser.add_argument('file2', help='file with the Variant genome')
parser.add_argument("-k","--kmerlength", dest = 'k', type=int, default = 50, help="length of the kmer")
args = parser.parse_args()


k = args.k

## ---  READING THE FILE -- ##

file1 = args.file1
file2 = args.file2


print(f"Start the computation:\n\tk-mer length:{k}\n\tWild file: {file1}\n\tVariant file: {file2}")

file1 = "salmonella-enterica.reads.fna"
file2 ="salmonella-enterica-variant.reads.fna"
k = 40





print(f"Processing file: {file1}") 
dictionary_wild = S.dictionary(file1, k)
S.plot_occurences((S.occurences(dictionary_wild)))
print(f"Removing the sequence errors of: {file1}")
S.remove_seq_errors(dictionary_wild, 10)

print("-----------------------------------------------------------------------------------")

print(f"Processing file: {file2}")
dictionary_variant = S.dictionary(file2, k)
S.plot_occurences((S.occurences(dictionary_variant)))


print(f"Removing the sequence errors of: {file2}")
S.remove_seq_errors(dictionary_variant, 10)
print("-----------------------------------------------------------------------------------")

print("\nRemoving the equal keys of both files")


wild_dict_final, variant_dict_final =S.remove_equal_keys(dictionary_wild, dictionary_variant)

print("\n Assembler of the filterd_wild")
keys_list = list(wild_dict_final.keys())

# final list of maxumal assembled genome pieces 
assemble_wild = []
no_intersection = keys_list

while set(assemble_wild) != set(no_intersection): 
    first_seq, no_intersection = S.assembler(no_intersection,10)
    if first_seq not in assemble_wild:
        assemble_wild.append(first_seq)
    no_intersection.append(first_seq)
    
print(assemble_wild)



print("\n Assembler of the filterd_variant")
keys_list = list(variant_dict_final.keys())

# final list of maxumal assembled genome pieces 
assemble_variant = []
no_intersection = keys_list

while set(assemble_variant) != set(no_intersection): 
    first_seq, no_intersection = S.assembler(no_intersection,10)
    if first_seq not in assemble_wild:
        assemble_variant.append(first_seq)
    no_intersection.append(first_seq)

print(assemble_variant)


# based on the Levestein distance between the few sequences we remain with, we compute the "nearest" ones, 
# that differ only for the final mutation
print("\n Getting the mutations:")
index = []
distances = []
for i in range(len(assemble_wild)):
    for j in range(len(assemble_variant)):
        if len(assemble_wild[i]) == len(assemble_variant[j]):
            index.append((i,j,))
            distances.append(S.distance(assemble_wild[i],assemble_variant[j]))

mutations = []
min_value = min(distances)
min_value
for i in range(len(distances)):
    if min_value == distances[i]:
        mutations.append((assemble_wild[index[i][0]], assemble_variant[index[i][1]]))




print(S.color_mutations(mutations))





    





 







    